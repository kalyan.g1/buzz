<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    $fields = 'partnerID, partnerName';

    $query = "SELECT $fields FROM partner where status = 1 ORDER BY partnerID desc, partnerName";
    
    $result = mysqli_query($con,$query);
    
    $row = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $response['list'] = $row;
    
    $response['code'] = 200;
    
    return showResponse($response,"successfully",true);    
    
?>  