<?php
   
    include 'common.php';

    $response = array();

    $fields = 'id, first_name';

    $query = "SELECT $fields FROM employee where status = 1 and empRole = 7 ORDER BY id desc";
    
    $result = mysqli_query($con,$query);
    
    $row = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $response['list'] = $row;
    
    $response['code'] = 200;
    
    return showResponse($response,"successfully",true);   
    
?>  