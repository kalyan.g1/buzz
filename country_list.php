<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    $query = "SELECT * FROM country_master ORDER BY country_name";
    
    $result = mysqli_query($con,$query);
    
    if($result->num_rows > 0)
    {
    
        $row = mysqli_fetch_all($result,MYSQLI_ASSOC);

        $response['list'] = $row;
        
        $response['code'] = 200;
        
        return showResponse($response,"successfully",true);   
    }
    else
    {   
    
        $response['code'] = 404;
        return showResponse($response,"Not found",false);   
    }    
    
?>  