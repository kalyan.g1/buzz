<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    
    if (isset($request['email'])){
        
        $email = $request['email'];
        
        if ($request['profile_pic'])
        {
            $profile_pic = $request['profile_pic'];
            
            $profile_query = "UPDATE employee SET profile_pic = '$profile_pic' WHERE officeMailId= '$email'";
        
            $data = mysqli_query($con,$profile_query);

        }
        
        $fields = "emp.id, emp.first_name, emp.last_name, emp.profile_pic, emp.empRole as role, rm.roleName as role_name";
        
        $query = "SELECT $fields  FROM employee emp left join roles_Master rm ON emp.empRole = rm.id where emp.officeMailId = '$email' and emp.status = 1";
        
        $result = mysqli_query($con,$query);

        if($result->num_rows > 0)
        {
           
            $data = mysqli_fetch_assoc($result);
            
            //check senior trainer
            if($data['role'] == 5)
            {
                $id = $data['id'];
                   
                $query = "SELECT id FROM employee where status = 1 and supervisorId = $id";
        
                $result = mysqli_query($con,$query);
            
                if(mysqli_num_rows($result) > 0)
                {
                     
                    $data['trainer_type'] = "senior"; 
            
                }
                
            }
            $response = $data;
            $response['code'] = 200;
            return showResponse($response,"successfully",true);   
        }
        else
        {   
            
            $response['code'] = 404;
            return showResponse($response,"Not found",false);   
        }
    }
?>  