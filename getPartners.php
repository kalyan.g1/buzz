<?php
include 'common.php';

$data = file_get_contents('php://input');
$request = json_decode($data, true);
$response = array();


    if (isset($request['pageNum'])) {
        $pageno = $request['pageNum'];
    }
    else {
        $pageno = 1;
    }
    
    $no_of_records = 25;
    $offset = ($pageno-1) * $no_of_records;
    
    $total_pages_sql = "SELECT COUNT(*) FROM partner where status = 1";
    $result = mysqli_query($con,$total_pages_sql);
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $no_of_records);
    
    $fields = "partnerID as id, partnerName as name";
    $query = "SELECT $fields FROM partner where status = 1 ORDER BY partnerName LIMIT $offset, $no_of_records";
    $result = mysqli_query($con,$query);
    
    $data = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $response['list'] = $data;
    $response['code'] = 200;
    
    return showResponse($response,"successfully",true);


?>