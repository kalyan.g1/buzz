<?php
include 'connection.php';

function showResponse($response, $message, $isSuccess)
{
    $response['success'] = $isSuccess;
    $response['message'] = $message;
    echo json_encode($response);
}

function getAutoIncrementValue($con, $Database, $tableName)
{
    $AutoIncrement = "";
    $Query = "SHOW TABLE STATUS FROM " . $Database;
    $Result = mysqli_query($con, $Query);
    if ($Result->num_rows > 0) {
        while ($row = $Result->fetch_assoc()) {
            if ($row["Name"] == $tableName) {
                $AutoIncrement = $row["Auto_increment"];
                if (is_null($AutoIncrement)) {
                    $AutoIncrement = 1;
                }
            }
        }
    }
    return $AutoIncrement;
}

?>