<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    $query = "SELECT * FROM roles_Master where id NOT IN ('1', '10') ORDER BY id";
    
    $result = mysqli_query($con,$query);
    
    $row = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $response['list'] = $row;
    
    $response['code'] = 200;
    
    return showResponse($response,"successfully",true);   
    
?>  