<?php
include 'common.php';

$response = array();

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);

$data = file_get_contents('php://input');
$request = json_decode($data, true);

//print_r($request);exit;*/
        /*$headers = array(
            'Content-Type: application/json'
        );

        $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL, 'https://bitmapp.in/test.php');
             curl_setopt($ch, CURLOPT_POST, true);
             curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

             $data = json_encode($_POST);

             curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
             $result = curl_exec($ch);
             curl_close($ch);
             exit;*/


if ($request)
{
    $countryID      = isset($request['countryID']) ? $request['countryID'] : NULL;
    $first_name     = isset($request['first_name']) ? $request['first_name'] : NULL;
    $last_name      = isset($request['last_name']) ? $request['last_name'] : NULL;
    $gender         = isset($request['gender']) ? $request['gender'] : NULL;
    $doj            = isset($request['doj']) ? date('Y-m-d',strtotime($request['doj'])) : date("Y-m-d");
    $officeMailId   = isset($request['officeMailId']) ? $request['officeMailId'] : NULL;
    $personalMailId = isset($request['personalMailId']) ? $request['personalMailId'] : NULL;
    $contactNum     = isset($request['contactNum']) ? $request['contactNum'] : NULL;
    $address        = isset($request['address']) ? $request['address'] : NULL;
    $address2       = isset($request['address2']) ? $request['address2'] : NULL;
    $address3       = isset($request['address3']) ? $request['address3'] : NULL;
    $pincode        = isset($request['pincode']) ? $request['pincode'] : NULL;
    $empRole        = isset($request['empRole']) ? $request['empRole'] : NULL;
    $supervisorId   = isset($request['supervisorId']) ? $request['supervisorId'] : NULL;
    $profile_pic    = isset($request['profile_pic']) ? $request['profile_pic'] : NULL;
    
    $status         = isset($request['status']) ? $request['status'] : NULL;
    $createdBy      = isset($request['createdBy']) ? $request['createdBy'] : NULL;
    $lastUpdatedBy  = isset($request['lastUpdatedBy']) ? $request['lastUpdatedBy'] : NULL;
    
    mysqli_autocommit($con,FALSE);

    try 
    {
        $insertQuery = "INSERT INTO employee(countryID, first_name, last_name, gender, doj, officeMailId, personalMailId, contactNum, address, address2, address3, pincode, empRole, supervisorId, profile_pic, status, createdBy, lastUpdatedBy) values ('$countryID','$first_name','$last_name','$gender','$doj','$officeMailId','$personalMailId','$contactNum','$address', '$address2', '$address3', '$pincode', '$empRole', '$supervisorId', '$profile_pic', '$status', '$createdBy', '$lastUpdatedBy')";
       
        if (mysqli_query($con,$insertQuery))
        {
        
            $emp_id = $con->insert_id;

            //insert into project_emps table
            
            if(!empty($request['project_list']))
            {
                $project_list = $request['project_list'];
                foreach($project_list as $val)
                {
                    $project_id = $val;

                    $project_emp_InsertQuery = "INSERT INTO project_emps(emp_id, project_id, role_id) values ('$emp_id','$project_id', '$empRole')";

                    mysqli_query($con,$project_emp_InsertQuery);

                    $project_emp_hist_InsertQuery = "INSERT INTO project_emps_history(emp_id, projectId, role_id) values ('$emp_id','$project_id', '$empRole')";   
        
                    mysqli_query($con,$project_emp_hist_InsertQuery);
                    
                }    
            }
        }
        mysqli_commit($con);
        showResponse($response,"added successfully",true);
    
    } catch (Exception $e) {
        
        mysqli_rollback($con);
        showResponse($response,"added unsuccessfully",false);
   
    }

}

?>