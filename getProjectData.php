<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    

    if(isset($request['id']))
    {
        $id = $request['id'];

        $fields = 'prj.id as project_id, prj.projectName as project_name, par.partnerName, prj.startDate, prj.endDate, prj.training_target, prj_emp.emp_id, prj_emp.role_id, prj.operations_manager as operations_manager_id, emp_a.first_name as operations_manager_name, prj.driverId, emp_b.first_name as driver_name, emp_c.first_name as emp_name, loc.id as location_id, loc.name as location_name';

        $query = "SELECT $fields FROM project prj left join partner par on prj.partnerID=par.partnerID left join project_emps prj_emp on prj.id = prj_emp.project_id left join employee emp_a on prj.operations_manager = emp_a.id left join employee emp_b on prj.driverId = emp_b.id left join employee emp_c on prj_emp.emp_id = emp_c.id left join location loc on prj.locationID = loc.id where prj.status = 1 and prj.id= $id group by prj_emp.emp_id ORDER BY projectName";
        

        $result = mysqli_query($con,$query);
        
        $data = mysqli_fetch_all($result,MYSQLI_ASSOC);

        $view = array(); 
        $view['trainers'] = array();
        $view['gelathiFacilitator'] = array();
        
        if(!empty($data))
        {
            foreach($data as $key => $val)
            {
                 $view['project_id']   = $val['project_id'];
                 $view['project_name'] = $val['project_name'];
                 $view['partnerName']  = $val['partnerName'];
                 $view['endDate']      = $val['endDate'];
                 $view['startDate']    = $val['startDate'];
                 $view['training_target']    = $val['training_target'];
                 $view['operations_manager_id']    = $val['operations_manager_id'];
                 $view['operations_manager_name']    = $val['operations_manager_name'];
                 $view['driverId']     = $val['driverId'];
                 $view['driver_name']  = $val['driver_name'];
                 $view['location_name']  = $val['location_name'];
                 if($val['role_id'] == 5)
                 {
                    $view['trainers'][$key]['emp_id'] = $val['emp_id'];
                    $view['trainers'][$key]['name'] = $val['emp_name'];
    
                 }else
                 {
                   $view['gelathiFacilitator'][$key]['emp_id'] = $val['emp_id'];
                   $view['gelathiFacilitator'][$key]['name'] = $val['emp_name'];
                 }
                 
            }    
                $view['trainers_count']           = count($view['trainers']);
                $view['gelathiFacilitator_count'] = count($view['gelathiFacilitator']);
         }
        
        $response['list'] = $view;
        
        $response['code'] = 200;
        
        return showResponse($response,"successfully",true);
    }
    
       
?>  