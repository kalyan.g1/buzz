<?php
include 'common.php';

$data = file_get_contents('php://input');
$request = json_decode($data, true);
$response = array();


    if (isset($request['pageNum'])) {
        $pageno = $request['pageNum'];
    }
    else {
        $pageno = 1;
    }
    
    $no_of_records = 25;
    $offset = ($pageno-1) * $no_of_records;
    
    // search by name start here
    $search = "";
    if(isset($request['name']))
    {
        $name   = $request['name'];

        $search = "and first_name LIKE '%$name%'";
    }

    if(isset($request['role_id']))
    {
        $role_id = $request['role_id'];
        $total_pages_sql = "SELECT COUNT(*) FROM employee where status = 1 and empRole = $role_id $search";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records);
        
        $fields = "id as id, first_name as first_name, last_name as last_name, empRole as role_id";
        $query = "SELECT $fields FROM employee where status = 1 and empRole = $role_id $search ORDER BY id desc LIMIT $offset, $no_of_records";
        $result = mysqli_query($con,$query);
        
        $data = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $response['list'] = $data;
        $response['code'] = 200;
        $response['total_count'] = $total_rows;
        
        return showResponse($response,"successfully",true);
    }
    


?>