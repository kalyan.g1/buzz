<?php
include 'common.php';

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);
$response = array();

if (isset($_POST['id']))
{
    $id             = $_POST['id'];
    $countryID      = isset($_POST['countryID']) ? $_POST['countryID'] : NULL;
    $partnerName    = isset($_POST['name']) ? $_POST['name'] : NULL;
    $phone          = isset($_POST['phone']) ? $_POST['phone'] : NULL;
    $mobilePhone    = isset($_POST['mobilePhone']) ? $_POST['mobilePhone'] : NULL;
    $emailID        = isset($_POST['emailID']) ? $_POST['emailID'] : NULL;
    $address        = isset($_POST['address']) ? $_POST['address'] : NULL;
    $status        = isset($_POST['status']) ? $_POST['status'] : NULL;
    $city           = isset($_POST['city']) ? $_POST['city'] : NULL;
    $district       = isset($_POST['district']) ? $_POST['district'] : NULL;
    $state          = isset($_POST['state']) ? $_POST['state'] : NULL;
    $pincode        = isset($_POST['pincode']) ? $_POST['pincode'] : NULL;
    $designation    = isset($_POST['designation']) ? $_POST['designation'] : NULL;
    $createdBy      = isset($_POST['createdBy']) ? $_POST['createdBy'] : NULL;
    $lastUpdatedBy  = isset($_POST['lastUpdatedBy']) ? $_POST['lastUpdatedBy'] : NULL;
    
    mysqli_autocommit($con,FALSE);
    try 
    {
        $updateQuery = "UPDATE partner SET countryID = '$countryID', partnerName = '$partnerName', phone = '$phone',
        mobilePhone = '$mobilePhone', emailID = '$emailID', address = '$address', status = '$status', city = '$city', district = '$district', state = '$state', pincode = '$pincode', designation = '$designation', createdBy = '$createdBy', lastUpdatedBy = '$lastUpdatedBy' WHERE partnerID = '$id'";
        
        if (mysqli_query($con,$updateQuery))
        {
            mysqli_commit($con);
            showResponse($response,"updated successfully",true);
        }
    }
    catch (Exception $e){
        
        mysqli_rollback($con);
        showResponse($response,"update failed",false);
   
    }     
    
}

?>