<?php

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);


include 'common.php';

$data = file_get_contents('php://input');
$request = json_decode($data, true);
$response = array();


if (isset($request['pageNum']))
{
    $pageno = $request['pageNum'];
}
else 
{
    $pageno = 1;
}

$no_of_records = 25;
$offset = ($pageno-1) * $no_of_records;


if(isset($request['user_id']) && isset($request['role_id']))
{   
    $user_id = $request['user_id'];
    $role_id = $request['role_id']; 
    
    // Admin part it will show all peoples   
          
        $filter_id = $request['filter_id'];
        
        if($filter_id == 0){
            $role = "and emp.empRole IN ('1','2','3','4','5','6','7')";    
        }
        elseif($filter_id == 10)
        {
            $role = "and emp.empRole IN ('1','2','3','4')"; 
        }
        else
        {
            $role = "and emp.empRole = $filter_id";
        }



    if($role_id == 2 || $role_id == 1)//Admin or Ceo list 
    {   
        
        // get all peoples for admin role
        $total_pages_sql = "SELECT COUNT(*) FROM employee emp where status = 1 and emp.id !=$user_id $role";

        $fields = "emp.id, emp.first_name, emp.last_name, emp.empRole as role_id, emp.officeMailId, emp.contactNum, rm.roleName as role_name, emp.profile_pic, emp.doj, emp.address, emp.address2, emp.address3,
        CONCAT(b.first_name,' ',b.last_name) AS supervisorName, emp.supervisorId, emp.status, emp.pincode";

        $query = "SELECT $fields FROM employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status = 1 and emp.id !=$user_id $role ORDER BY emp.first_name LIMIT $offset, $no_of_records";
        
            //To fetch role partner or funder

            if($filter_id == 8 || $filter_id == 9)
            {
               
                if($filter_id == 8)
                {
                    $total_pages_sql = "SELECT COUNT(*) FROM funder where status = 1";
                    
                    $fields = "funderID as id, funderName AS first_name, NULL AS last_name, emailID as officeMailId, workPhone as contactNum, 8 as role_id, 'funder' as role_name, true as profile_pic, address, status, pincode";
                    
                    $query = "SELECT $fields FROM funder where status = 1 ORDER BY funderName LIMIT $offset, $no_of_records";
                }
                elseif($filter_id == 9)
                {
                    $total_pages_sql = "SELECT COUNT(*) FROM partner where status = 1";
                    $fields = "partnerID as id, partnerName AS first_name, NULL AS last_name, emailID as officeMailId, phone as contactNum, 9 as role_id, 'partner' as role_name, true as profile_pic, address, status, pincode";
                    
                    $query = "SELECT $fields FROM partner where status = 1 ORDER BY partnerName LIMIT $offset, $no_of_records";
                }    

                $result = mysqli_query($con,$total_pages_sql);
                $total_rows = mysqli_fetch_array($result)[0];
                $total_pages = ceil($total_rows / $no_of_records);

                $result = mysqli_query($con,$query);
                
                $data = mysqli_fetch_all($result,MYSQLI_ASSOC);
                
                foreach($data as $key => $val){
                    $data[$key]['project_list'] = array();
                }
                
                $response['list'] = $data;
                
                
                $response['total_count'] = $total_rows;
                $response['code'] = 200;
                
                return showResponse($response,"successfully",true);
            }

    }

    $getIds = "SELECT GROUP_CONCAT(Level SEPARATOR ',')  FROM (SELECT @Ids := (SELECT GROUP_CONCAT(`ID` SEPARATOR ',')FROM employee WHERE FIND_IN_SET(`supervisorId`, @Ids)) Level FROM employee JOIN (SELECT @Ids := $user_id) temp1) temp2";

    $result = mysqli_query($con,$getIds);
    $data   = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $ids = implode(',', $data);

    if($role_id == 3)//program manager
    {

        $total_pages_sql = "SELECT count(*) from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status =1  and emp.id IN($ids) $role";

        $fields = "emp.id, emp.first_name, emp.last_name, emp.empRole as role_id, emp.officeMailId, emp.contactNum, rm.roleName as role_name, emp.profile_pic, emp.doj, emp.address, emp.address2, emp.address3,
        CONCAT(b.first_name,' ',b.last_name) AS supervisorName, emp.supervisorId, emp.status, emp.pincode";

        $query = "SELECT $fields from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status =1  and emp.id IN($ids) $role ORDER BY emp.first_name LIMIT $offset, $no_of_records";
        
    }

    if($role_id == 4)// Operation Manager = 4
    {
        $total_pages_sql = "SELECT count(*) from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status = 1 and emp.id IN($ids) $role";

        $fields = "emp.id, emp.first_name, emp.last_name, emp.empRole as role_id, emp.officeMailId, emp.contactNum, rm.roleName as role_name, emp.profile_pic, emp.doj, emp.address, emp.address2, emp.address3,
        CONCAT(b.first_name,' ',b.last_name) AS supervisorName, emp.supervisorId, emp.status, emp.pincode";

        $query = "SELECT $fields from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status = 1 and emp.id IN($ids) $role ORDER BY emp.first_name LIMIT $offset, $no_of_records";
    }

    if($role_id == 5)// if senior trainer 
    {
        $type = $request['type'];
        if($type == 'senior')
        {
            $total_pages_sql = "SELECT count(*) from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status = 1 and emp.id IN($ids)";

            $fields = "emp.id, emp.first_name, emp.last_name, emp.empRole as role_id, emp.officeMailId, emp.contactNum, rm.roleName as role_name, emp.profile_pic, emp.doj, emp.address, emp.address2, emp.address3,CONCAT(b.first_name,' ',b.last_name) AS supervisorName, emp.supervisorId, emp.status, emp.pincode";

            $query = "SELECT $fields from employee emp left join employee b on b.id = emp.supervisorId left join roles_Master rm on emp.empRole = rm.id where emp.status = 1 and emp.id IN($ids) ORDER BY emp.first_name LIMIT $offset, $no_of_records";
            
        }
        
    }

}

    $result = mysqli_query($con,$total_pages_sql);
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $no_of_records);

    $result = mysqli_query($con,$query);
    $data   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $response['list'] = $data;

    //fetching projects for the user
    foreach($response['list'] as $key => $val)
    {
        $user_id = $val['id'];
        $fields = "em_pr.*, pr.projectName";
        $query_project = "SELECT $fields from project_emps em_pr left join project pr on em_pr.project_id = pr.id where emp_id IN ($user_id)";    
       
        $project_result = mysqli_query($con,$query_project);
        $projects = mysqli_fetch_all($project_result,MYSQLI_ASSOC);
        $response['list'][$key]['project_list'] = $projects;
    }

    $response['total_count'] = $total_rows;
    $response['code'] = 200;

    return showResponse($response,"successfully",true);
    
?>