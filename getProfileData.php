<?php
    
    include 'common.php';

    $data = file_get_contents('php://input');
    $request = json_decode($data, true);
    $response = array();

    
    if (isset($request['id'])){
            
        $id = $request['id'];
        
        $fields = "a.id, a.countryID, a.first_name, a.last_name,  a.gender, a.doj, a.officeMailId, a.personalMailId, a.contactNum, a.address, a.address2, a.address3, a.empRole as role_id, a.supervisorId, a.profile_pic, a.status, a.pincode,  CONCAT(b.first_name, ' ', b.last_name) AS supervisorName, rm.roleName as role_name";
        
        
        $query = "SELECT $fields  FROM employee a left join employee b on b.id = a.supervisorId left join roles_Master rm on rm.id = a.empRole where a.id = '$id' and a.status = 1";
        
        $result = mysqli_query($con,$query);
        
        if($result->num_rows > 0)
        {
           
            $data = mysqli_fetch_assoc($result);
            $response = $data;
            
            $user_id = $data['id'];
            //project for that employee
            $fields = "em_pr.*, pr.projectName";
            $query_project = "SELECT $fields from project_emps em_pr left join project pr on em_pr.project_id = pr.id where emp_id IN ($user_id)";    
            
            $project_result = mysqli_query($con,$query_project);
            $projects = mysqli_fetch_all($project_result,MYSQLI_ASSOC);
            $response['project_list'] = $projects;
            
            $response['code'] = 200;
            
            return showResponse($response,"successfully",true);   
        }
        else
        {   
            $response['code'] = 404;
            return showResponse($response,"Not found",false);   

        }    
    
    }
?>  