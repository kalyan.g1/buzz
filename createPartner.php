<?php
include 'common.php';

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);
$response = array();

if (isset($_POST))
{

    $countryID      = isset($_POST['countryID']) ? $_POST['countryID'] : NULL;
    $partnerName    = isset($_POST['name']) ? $_POST['name'] : NULL;
    $phone          = isset($_POST['phone']) ? $_POST['phone'] : NULL;
    $mobilePhone    = isset($_POST['mobilePhone']) ? $_POST['mobilePhone'] : NULL;
    $emailID        = isset($_POST['emailID']) ? $_POST['emailID'] : NULL;
    $address        = isset($_POST['address']) ? $_POST['address'] : NULL;
    $status         = isset($_POST['status']) ? $_POST['status'] : NULL;
    $city           = isset($_POST['city']) ? $_POST['city'] : NULL;
    $district       = isset($_POST['district']) ? $_POST['district'] : NULL;
    $state          = isset($_POST['state']) ? $_POST['state'] : NULL;
    $pincode        = isset($_POST['pincode']) ? $_POST['pincode'] : NULL;
    $designation    = isset($_POST['designation']) ? $_POST['designation'] : NULL;
    $createdBy      = isset($_POST['createdBy']) ? $_POST['createdBy'] : NULL;
    $lastUpdatedBy  = isset($_POST['lastUpdatedBy']) ? $_POST['lastUpdatedBy'] : NULL;
    
    mysqli_autocommit($con,FALSE);

    try{

        $insertQuery = "INSERT INTO partner (countryID, partnerName, phone, mobilePhone, emailID, address, status, city, district, state, pincode, designation, createdBy, lastUpdatedBy) values ('$countryID', '$partnerName', '$phone', '$mobilePhone', '$emailID', '$address', '$status', '$city', '$district', '$state', '$pincode', '$designation', '$createdBy', '$lastUpdatedBy')";
        
        if (mysqli_query($con,$insertQuery))
        {
            mysqli_commit($con);
            showResponse($response,"added successfully",true);
        }

    }
    catch(Exception $e){
        mysqli_rollback($con);
        showResponse($response,"added unsuccessfully",false);
    }
    
}
?>
