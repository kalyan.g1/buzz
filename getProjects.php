<?php

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);

include 'common.php';

$data = file_get_contents('php://input');
$request = json_decode($data, true);
$response = array();

    
if (isset($request['pageNum']))
{
    $pageno = $request['pageNum'];
}
else {
    $pageno = 1;
}

$no_of_records = 25;
$offset = ($pageno-1) * $no_of_records;

if (isset($request['id']))
{
    $id = $request['id'];

    $fields = 'empRole';

    $query = "SELECT $fields FROM employee where status = 1 and id = $id";
    
    $result = mysqli_query($con,$query);
    
    $data = mysqli_fetch_assoc($result);
    
    $role_id = $data['empRole'];
}

 
if($role_id == 2)//Admin = 2
{

    $total_pages_sql = "SELECT COUNT(*) FROM project where status = 1";
    
    $fields = 'prj.id, prj.projectName as name, CASE WHEN prj.project_status = "0" THEN "Inprogress" WHEN prj.project_status = "1" THEN "Published" ELSE "Completed" END AS project_status_name, project_status,loc.name as location_name';

    $query = "SELECT $fields FROM project prj left join location loc on prj.locationID = loc.id where prj.status = 1 ORDER BY prj.id desc LIMIT $offset, $no_of_records";
    
}
elseif($role_id == 3)// Program Manager = 3
{
    $total_pages_sql = "SELECT COUNT(*) FROM employee emp left join project pr on pr.operations_manager = emp.id where emp.supervisorId = $id and emp.status =1 and pr.status = 1 and pr.project_status = '1'";
    
    $fields = 'emp.id as emp_id, pr.id as project_id, pr.projectName as project_name, NULL AS project_status_name, NULL as project_status, loc.name as location_name';

    $query = "SELECT $fields from employee emp left join project pr on pr.operations_manager = emp.id left join location loc on pr.locationID = loc.id where emp.supervisorId = $id and emp.status =1 and pr.status = 1 and pr.project_status = '1'";
    

}
elseif($role_id == 4)// Operation Manager = 4
{
    $total_pages_sql = "SELECT COUNT(*) FROM project pr where pr.operations_manager = $id and pr.status = 1 and pr.project_status = '1'";
    
    $fields = 'pr.id as project_id, pr.projectName as project_name, NULL AS project_status_name, NULL as project_status, loc.name as location_name';

    $query = "SELECT $fields from project pr left join location loc on pr.locationID = loc.id where pr.operations_manager = $id and pr.status = 1 and pr.project_status = '1'";
    

}
elseif($role_id == 5 || $role_id == 6 || $role_id == 7)// Trainer = 5, GelathiFacilitator = 6, Driver = 7
{
    $fields = "";
    //Finding trainer as a senior trainer
    if($role_id == 5)
    {
        $query = "SELECT id FROM employee where status = 1 and supervisorId = $id";
        
        $result = mysqli_query($con,$query);
    
        if(mysqli_num_rows($result) > 0)
        {
             
             $fields .= "'senior' as trainer_type,"; 
    
        }    
        
    }

    $total_pages_sql = "SELECT COUNT(*) FROM project_emps pr_emp left join project pr on pr_emp.project_id = pr.id where pr_emp.emp_id = $id";
    
    $fields .= 'pr_emp.project_id, pr.projectName as project_name, NULL AS project_status_name, NULL as project_status, loc.name as location_name';

    $query = "SELECT $fields from project_emps pr_emp left join project pr  on pr_emp.project_id = pr.id left join location loc on pr.locationID = loc.id where pr_emp.emp_id = $id";
    
}


$result = mysqli_query($con,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records);

$result = mysqli_query($con,$query);

$data = mysqli_fetch_all($result,MYSQLI_ASSOC);
$response['list']   = $data;
$response['count']  = $total_rows;
$response['code']   = 200;

return showResponse($response,"successfully",true);

?>