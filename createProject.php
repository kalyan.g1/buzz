<?php
include 'common.php';

$response = array();

// create incomplete project
if((!empty($_POST['locationID']) && !empty($_POST['funderID'])) && !isset($_POST['PartnerId'], $_POST['OperationsManger'], $_POST['busId'], $_POST['driverId']))
{
    $locationID     = $_POST['locationID'];
    //$location_name  = $_POST['location_name'];

    $funderID       = $_POST['funderID'];
   //$funder_name    = $_POST['funder_name'];
    
    $createdBy      = $_POST['createdBy'];

    $lastUpdatedBy  = $_POST['lastUpdatedBy'];

    try
    {

        $insertQuery = "INSERT INTO project (funderID, locationID, createdBy, lastUpdatedBy) values ('$funderID', '$locationID', $createdBy, $lastUpdatedBy)";
        
        if (mysqli_query($con,$insertQuery))
        {
            $inserted_id = mysqli_insert_id($con);

            $projectName = generateProjectName($locationID, $funderID, $inserted_id);

            if(updateProjectID($con, $inserted_id, $projectName))
            {
                mysqli_commit($con);
                showResponse($response,"added successfully",true);      
            }
            
        }

    }
    catch(Exception $e){
        mysqli_rollback($con);
        showResponse($response,"added unsuccessfully",false);
    }
}
else
{
    // publish the incompelete project
    if(isset($_POST['project_id']))
    {
        $project_id         = $_POST['project_id'];
        $partnerID          = $_POST['partnerID'];
        $operations_manager = $_POST['operations_manager'];
        $busID              = $_POST['busID'];
        $driverID           = $_POST['driverID'];

        $project_people     = $_POST['project_people'];

        $updateQuery = "UPDATE project SET partnerID = '$partnerID', operations_manager = '$operations_manager', busID = '$busID', driverID = '$driverID', project_status = '1' WHERE id = '$project_id'";
        
        if (mysqli_query($con,$updateQuery) && insertProjectEmployee($con,$project_people))
        {
            mysqli_commit($con);
            showResponse($response,"updated successfully",true);
        }
         
    }
    // publish the project
    else
    {
        $funderID             = $_POST['funderID'];
        $partnerID            = $_POST['partnerID'];
        $locationID           = $_POST['locationID'];
        $operations_manager   = $_POST['operations_manager'];
        $busID                = $_POST['busID'];
        $driverID             = $_POST['driverID'];
        $project_status       = $_POST['project_status'];
        $createdBy            = $_POST['createdBy'];
        $lastUpdatedBy        = $_POST['lastUpdatedBy'];

        $project_people        = $_POST['project_people'];

        try
        {

            $insertQuery = "INSERT INTO project (funderID, partnerID, locationID, operations_manager, busID, driverID, project_status, createdBy, lastUpdatedBy) values ('$funderID', '$partnerID', $locationID, $operations_manager, $busID, $driverID, $project_status, $createdBy, $lastUpdatedBy)";
            
            if (mysqli_query($con,$insertQuery))
            {
                $inserted_id = mysqli_insert_id($con);

                $projectName = generateProjectName($locationID, $funderID, $inserted_id);

                if(updateProjectID($con, $inserted_id, $projectName) && insertProjectEmployee($con,$project_people))
                {
                    mysqli_commit($con);
                    showResponse($response,"added successfully",true);      
                }
                
            }

        }
        catch(Exception $e){
            mysqli_rollback($con);
            showResponse($response,"added unsuccessfully",false);
        }

    }
 
}


// Generate Project name
function generateProjectName($location, $funder, $insertedId)
{

    $funder = substr($funder,0,2);
    $year   = substr(date("y"), -2);
   
    $projectName = $location.$funder.$year.$insertedId;

    return $projectName;
}

// Project Id update into table
function updateProjectID($con, $id, $projectName)
{

    $updatetQuery = "UPDATE project SET projectName = '$projectName' where id = $inserted_id";
                    
    if(mysqli_query($con,$updatetQuery))
    {
        return true;     
    }
}

// inserting project employee
function insertProjectEmployee($con, $project_people)
{   
    foreach($project_people as $val)
    {
        $emp_id     = $val['emp_id'];
        $project_id = $val['project_id'];
        $role_id    = $val['role_id'];

        $project_emp_InsertQuery = "INSERT INTO project_emps(emp_id, project_id, role_id) values ('$emp_id','$project_id, $role_id')";


        $project_emp_hist_InsertQuery = "INSERT INTO project_emps_history(emp_id, projectId, role_id) values ('$emp_id', '$project_id, $role_id')";   

        
        
        if(mysqli_query($con,$project_emp_InsertQuery) && mysqli_query($con,$project_emp_hist_InsertQuery))
        {
            return true;
        }

    }  
    
} 

?>
