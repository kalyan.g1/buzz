<?php
include 'common.php';

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);

$response = array();

$data = file_get_contents('php://input');
$request = json_decode($data, true);

if ($request)
{

    $id             = $request['id'];

    $countryID      = isset($request['countryID']) ? $request['countryID'] : NULL;
    $first_name     = isset($request['first_name']) ? $request['first_name'] : NULL;
    $last_name      = isset($request['last_name']) ? $request['last_name'] : NULL;
    $gender         = isset($request['gender']) ? $request['gender'] : NULL;
    $doj            = isset($request['doj']) ? date('Y-m-d',strtotime($request['doj'])) : date("Y-m-d");
    $officeMailId   = isset($request['officeMailId']) ? $request['officeMailId'] : NULL;
    $personalMailId = isset($request['personalMailId']) ? $request['personalMailId'] : NULL;
    $contactNum     = isset($request['contactNum']) ? $request['contactNum'] : NULL;
    $address        = isset($request['address']) ? $request['address'] : NULL;
    $address2       = isset($request['address2']) ? $request['address2'] : NULL;
    $address3       = isset($request['address3']) ? $request['address3'] : NULL;
    $pincode        = isset($request['pincode']) ? $request['pincode'] : NULL;
    $empRole        = isset($request['empRole']) ? $request['empRole'] : NULL;
    $supervisorId   = isset($request['supervisorId']) ? $request['supervisorId'] : NULL;
    $profile_pic    = isset($request['profile_pic']) ? $request['profile_pic'] : NULL;
    
    $status         = isset($request['status']) ? $request['status'] : NULL;
    $createdBy      = isset($request['createdBy']) ? $request['createdBy'] : NULL;
    $lastUpdatedBy  = isset($request['lastUpdatedBy']) ? $request['lastUpdatedBy'] : NULL;
    
    mysqli_autocommit($con,FALSE);

    try 
    {
    
        $updateQuery = "UPDATE employee SET countryID = '$countryID', first_name = '$first_name', last_name = '$last_name', gender = '$gender', doj = '$doj', officeMailId = '$officeMailId', personalMailId = '$personalMailId', contactNum = '$contactNum', address = '$address', address2 = '$address2', address3 = '$address3', pincode = '$pincode', empRole = '$empRole', supervisorId = '$supervisorId', profile_pic = '$profile_pic', status = '$status', createdBy = '$createdBy', lastUpdatedBy = '$lastUpdatedBy' WHERE id= '$id'";
            
        if (mysqli_query($con,$updateQuery))
        {
              
            if(!empty($request['project_list']))
            {
                
                $project_list = $request['project_list'];

                $query = "SELECT project_id FROM project_emps WHERE emp_id = '$id'";
                $count = mysqli_query($con,$query);
                $row = mysqli_fetch_all($count,MYSQLI_ASSOC);
                
                $project_exist = array();
               
                foreach($row as $val)
                {
                    
                    $project_exist[] = $val['project_id'];
                }    
                
                // update if role_id changed
                if(count($row) > 0)
                {
                    if($row[0]['role_id'] !== $empRole)
                    {
                        
                        $updateRole = "UPDATE project_emps SET role_id = $empRole WHERE emp_id = '$id'";    

                        mysqli_query($con,$updateRole);
                        
                        $project_Ids  = implode(",",$project_exist);

                        $updateRoleHistory= "UPDATE project_emps_history SET role_id = $empRole WHERE projectId IN ($project_Ids) and emp_id = '$id' and endDate IS NULL";  
                        
                        mysqli_query($con,$updateRoleHistory);
                        
                    }
                }
                
                $delete_result=array_diff($project_exist,$project_list);

                $update_result=array_diff($project_list,$project_exist);

                foreach($delete_result as $val)
                {
                    $project_id = $val;
                    //delete from table project_emps
                    $deleteQuery = "DELETE FROM project_emps WHERE project_id='$project_id' and emp_id = '$id'";
                    mysqli_query($con,$deleteQuery);

                    //update table project_emps_history
                    $updateHistory = "UPDATE project_emps_history SET endDate = current_date() WHERE projectId='$project_id' and emp_id = '$id' and role_id = $empRole";
                    mysqli_query($con,$updateHistory);

                }

                foreach($update_result as $val)
                {
                    $project_id = $val;
                    
                    $emp_insertQuery = "INSERT into project_emps(emp_id, project_id, role_id) values ('$id','$project_id', '$empRole')";
                    
                    mysqli_query($con,$emp_insertQuery);

                    $emp_hist_insertQuery = "INSERT into project_emps_history(emp_id, projectId, role_id) values ('$id','$project_id', '$empRole')";
                    
                    mysqli_query($con,$emp_hist_insertQuery);
                } 
                
            }
            else
            {
                $deleteQuery = "DELETE FROM project_emps WHERE emp_id = '$id'";
                mysqli_query($con,$deleteQuery);
                
                $updateHistory = "UPDATE project_emps_history SET endDate = current_date() WHERE emp_id = '$id'";
                mysqli_query($con,$updateHistory);
            }

        }

        mysqli_commit($con);
        showResponse($response,"updated successfully",true);
    }    
    catch (Exception $e){
        
        mysqli_rollback($con);
        showResponse($response,"update failed",false);
   
    } 
}
      
?>