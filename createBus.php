<?php
include 'common.php';
$response = array();

ini_set( 'display_errors', 0 );
error_reporting(E_ERROR | E_PARSE);

$data = file_get_contents('php://input');
$request = json_decode($data, true);

if (isset($request))
{

    $register_number        = isset($request['register_number']) ? $request['register_number'] : NULL;
    $register_date          = isset($request['register_date']) ? $request['register_date'] : NULL;
    $engine_number          = isset($request['engine_number']) ? $request['engine_number'] : NULL;
    $chassis_number         = isset($request['chassis_number']) ? $request['chassis_number'] : NULL;
    $insurance_number       = isset($request['insurance_number']) ? $request['insurance_number'] : NULL;
    $insurance_company      = isset($request['insurance_company']) ? $request['insurance_company'] : NULL;
    $insurance_start_date   = isset($request['insurance_start_date']) ? $request['insurance_start_date'] : NULL;
    $insurance_end_date     = isset($request['insurance_end_date']) ? $request['insurance_end_date'] : NULL;
    $last_service_date      = isset($request['last_service_date']) ? $request['last_service_date'] : NULL;
    $next_service_due_date  = isset($request['next_service_due_date']) ? $request['next_service_due_date'] : NULL;
    $createdBy              = isset($request['createdBy']) ? $request['createdBy'] : NULL;
    $lastUpdatedBy          = isset($request['lastUpdatedBy']) ? $request['lastUpdatedBy'] : NULL;
    

    mysqli_autocommit($con,FALSE);

    try{

         $insertQuery = "INSERT INTO bus (register_number, register_date, engine_number, chassis_number, insurance_number, insurance_company, insurance_start_date, insurance_end_date, last_service_date, next_service_due_date, createdBy, lastUpdatedBy) values ('$register_number','$register_date','$engine_number','$chassis_number','$insurance_number','$insurance_company', '$insurance_start_date','$insurance_end_date','$last_service_date', '$next_service_due_date', '$createdBy', '$lastUpdatedBy')";
        
        if (mysqli_query($con,$insertQuery))
        {
            mysqli_commit($con);
            showResponse($response,"added successfully",true);
        }

    }
    catch(Exception $e){
        mysqli_rollback($con);
        showResponse($response,"added unsuccessfully",false);
    }
}
?>
